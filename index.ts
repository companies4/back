import express from "express";
import * as dotenv from "dotenv";

dotenv.config();
const app = express();

app.get("/", (req, res) => {
  res.send("holamundo");
});

app.listen(process.env.PORT, () => console.log("Server running"))
